// Command functions for shell.

#pragma once

#include <stdbool.h>

#define MAX_SUB_COMMANDS 20
#define MAX_ARGS 20

struct SubCommand {
    char *line;
    char *argv[MAX_ARGS];
};

struct Command {
    struct SubCommand sub_cmds[MAX_SUB_COMMANDS];
    int num_sub_cmds;

    char *stdin_file;
    char *stdout_file;
    bool bg;
};

//===================================================================
// SubCommand API

// Default initialize a subcommand.
void subcommand_init(struct SubCommand *sub);

// Free memory allocated for subcommand.
void subcommand_free(struct SubCommand *sub);

// Print subcommand arguments.
void subcommand_print(struct SubCommand *sub);

//===================================================================
// Command API

// Default initialize a command.
void command_init(struct Command *cmd);

// Free memory allocated for command.
void command_free(struct Command *cmd);

// Print all subcommands that comprise this command.
void command_print(struct Command *cmd);

//===================================================================
// Parsing API

// Parse the input line into subcommands.
void read_command(char *line, struct Command *cmd);

// Parse the given input buffer for arguments.
// Uses strtok internally so watch out.
void read_argv(char *in, char **argv, int size);

// Parse last subcommand for redirect and background.
void read_launch_ops(struct Command *cmd);

