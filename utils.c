#include <stdlib.h>
#include <string.h>
#include "utils.h"

void trim_newline(char *buffer) {
    int last = strlen(buffer) - 1;
    if (buffer[last] == '\n') {
        buffer[last] = '\0';
    }
}
