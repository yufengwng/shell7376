`shell7376` is a minimal shell that I wrote for an operating systems class project.

It supports some limited features:

* basic command execution
* I/O redirect and piping
* background execution and reporting of completed background jobs
* prompt with user, hostname, and current directory
* buildin for `cd` and `exit`
* handles Ctrl-C signal to kill a running process
* handles Ctrl-D signal to exit shell

