// Utilities for shell.

#pragma once

// Removes any trailing newline in buffer.
void trim_newline(char *buffer);

