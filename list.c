#include <stdlib.h>
#include "list.h"

//===================================================================
// utils

void node_init(struct Node *nd) {
    nd->data = NULL;
    nd->next = NULL;
    nd->prev = NULL;
}

//===================================================================
// list behavior

void list_init(struct List *ls) {
    ls->head = NULL;
    ls->tail = NULL;
    ls->size = 0;
}

int list_free(struct List *ls) {
    int count = 0;
    struct Node *curr = ls->head;
    while (curr != NULL) {
        struct Node *next = curr->next;
        free(curr);
        count++;
        curr = next;
    }
    ls->head = NULL;
    ls->tail = NULL;
    ls->size = 0;
    return count;
}

struct Node* list_add(struct List *ls, int index, void *data) {
    if (index < 0 || index > ls->size) {
        return NULL;
    }

    struct Node *nd = malloc(sizeof(struct Node));
    node_init(nd);
    nd->data = data;

    if (ls->size == 0) {
        ls->head = nd;
        ls->tail = nd;
        ls->size += 1;
        return nd;
    }

    if (index == ls->size) {
        nd->prev = ls->tail;
        ls->tail->next = nd;
        ls->tail = nd;
        ls->size += 1;
        return nd;
    }

    int i;
    struct Node *curr = ls->head;
    for (i = 0; i < index; i++) {
        curr = curr->next;
    }

    nd->prev = curr->prev;
    nd->next = curr;
    curr->prev = nd;

    if (index == 0) {
        ls->head = nd;
    } else {
        nd->prev->next = nd;
    }

    return nd;
}

struct Node* list_del(struct List *ls, int index) {
    if (index < 0 || index >= ls->size) {
        return NULL;
    }

    int i;
    struct Node *curr = ls->head;
    for (i = 0; i < index; i++) {
        curr = curr->next;
    }

    if (ls->size == 1) {
        ls->head = NULL;
        ls->tail = NULL;
        ls->size -= 1;
        return curr;
    }

    if (index == 0) {
        ls->head = ls->head->next;
        ls->head->prev = NULL;
        curr->next = NULL;
        ls->size -= 1;
        return curr;
    }

    if (index + 1 == ls->size) {
        ls->tail = ls->tail->prev;
        ls->tail->next = NULL;
        curr->prev = NULL;
        ls->size -= 1;
        return curr;
    }

    curr->prev->next = curr->next;
    curr->next->prev = curr->prev;
    curr->next = NULL;
    curr->prev = NULL;
    ls->size -= 1;

    return curr;
}

struct Node* list_get(struct List *ls, int index) {
    if (index < 0 || index >= ls->size) {
        return NULL;
    }

    int i;
    struct Node *curr = ls->head;
    for (i = 0; i < index; i++) {
        curr = curr->next;
    }

    return curr;
}

//===================================================================
// stack-like behavior

struct Node* list_push(struct List *ls, void *data) {
    return list_add(ls, ls->size, data);
}

struct Node* list_pop(struct List *ls) {
    return list_del(ls, ls->size - 1);
}

//===================================================================
// queue-like behavior

struct Node* list_enqueue(struct List *ls, void *data) {
    return list_add(ls, 0, data);
}

struct Node* list_dequeue(struct List *ls) {
    return list_del(ls, 0);
}
