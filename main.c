// EECE 7376 Operating Systems
// Shell Project

#include <fcntl.h>
#include <pwd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include "commands.h"
#include "list.h"
#include "utils.h"

#define MAX_INPUT 512
#define MAX_HOSTNAME 100

// Free list of background pids.
void free_bg_pids(struct List *bg_pids) {
    int i;
    for (i = 0; i < bg_pids->size; i++) {
        int *id = list_get(bg_pids, i)->data;
        free(id);
    }
    list_free(bg_pids);
}

// Free list of pipe file descriptors.
void free_pipe_fds(struct List *pipe_fds) {
    int i;
    for (i = 0; i < pipe_fds->size; i++) {
        int *fds = list_get(pipe_fds, i)->data;
        close(fds[0]);
        close(fds[1]);
        free(fds);
    }
    list_free(pipe_fds);
}

// Check if any background processes has finished.
void check_bg_jobs(struct List *bg_pids) {
    if (bg_pids->size > 0) {
        int i;
        for (i = 0; i < bg_pids->size; i++) {
            int *id = list_get(bg_pids, i)->data;
            if (waitpid(*id, NULL, WNOHANG) > 0) {
                printf("[%d] finished\n", *id);
                struct Node *nd = list_del(bg_pids, i);
                free(nd->data);
                free(nd);
                break;
            }
        }
    }
}

// Get current working directory.
char* get_curr_dir() {
    char *cwd = getcwd(NULL, 0);
    if (cwd == NULL) {
        cwd = "";
    }
    return cwd;
}

// Handle buildin command for "cd".
void buildin_cd(char **args, char *home_dir) {
    char *dest_dir = args[1];
    if (dest_dir == NULL) {
        dest_dir = home_dir;
    }
    if (chdir(dest_dir) < 0) {
        perror("cd");
    }
}

// Handle buildin command for "exit".
void buildin_exit() {
    exit(0);
}

// Change stdin to use the opened file.
// Returns the file descriptor.
// Returns less than 0 on error.
int redirect_stdin(char *file) {
    int fd = open(file, O_RDONLY);
    if (fd >= 0) {
        close(STDIN_FILENO);
        dup(fd);
    }
    return fd;
}

// Change stdout to use the opened file.
// Returns the file descriptor.
// Returns less than 0 on error.
int redirect_stdout(char *file) {
    int fd= open(file, O_WRONLY | O_TRUNC | O_CREAT, 0644);
    if (fd >= 0) {
        close(STDOUT_FILENO);
        dup(fd);
    }
    return fd;
}

// Execute the child process depending on running context.
// Warning: this does not return unless there is error.
void execute_child(struct Command *cmd, struct List *pipe_fds, int index) {
    if (cmd->num_sub_cmds == 1) {
        if (cmd->stdin_file != NULL) {
            if (redirect_stdin(cmd->stdin_file) < 0) {
                fprintf(stderr, "%s: File not found\n", cmd->stdin_file);
                exit(1);
            }
        }

        if (cmd->stdout_file != NULL) {
            if (redirect_stdout(cmd->stdout_file) < 0) {
                fprintf(stderr, "%s: Cannot create file\n", cmd->stdout_file);
                exit(1);
            }
        }
    } else if (index == 0) {
        if (cmd->stdin_file != NULL) {
            if (redirect_stdin(cmd->stdin_file) < 0) {
                fprintf(stderr, "%s: File not found\n", cmd->stdin_file);
                exit(1);
            }
        }

        int *fds = list_get(pipe_fds, index)->data;
        close(STDOUT_FILENO);
        dup(fds[1]);
        close(fds[1]);
    } else if (index + 1 == cmd->num_sub_cmds) {
        if (cmd->stdout_file != NULL) {
            if (redirect_stdout(cmd->stdout_file) < 0) {
                fprintf(stderr, "%s: Cannot create file\n", cmd->stdout_file);
                exit(1);
            }
        }

        int *fds = list_get(pipe_fds, index - 1)->data;
        close(STDIN_FILENO);
        dup(fds[0]);
        close(fds[0]);
    } else {
        int *fds = list_get(pipe_fds, index - 1)->data;
        close(STDIN_FILENO);
        dup(fds[0]);
        close(fds[0]);

        fds = list_get(pipe_fds, index)->data;
        close(STDOUT_FILENO);
        dup(fds[1]);
        close(fds[1]);
    }

    char **args = cmd->sub_cmds[index].argv;
    execvp(args[0], args);
}

// Capture the SIGINT signal.
void handle_sigint(int signum) {
    // no-op
    signum = signum;
    // Print backspace to clear the ^C
    printf("\b\b");
}

int main() {
    signal(SIGINT, handle_sigint);

    char input[MAX_INPUT];
    char *delimiter = "$";

    // Get the username and home directory
    struct passwd *pwd = getpwuid(getuid());
    if (pwd == NULL) {
        fprintf(stderr, "shell: Unable to find user info\n");
        exit(1);
    }

    char *user = pwd->pw_name;
    char *home_dir = pwd->pw_dir;

    // Get the computer hostname
    char host[MAX_HOSTNAME];
    if (gethostname(host, MAX_HOSTNAME) < 0) {
        fprintf(stderr, "shell: Unable to get hostname\n");
        exit(1);
    }

    // Start an empty list of background pids
    struct List bg_pids;
    list_init(&bg_pids);

    while (true) {
        char *cwd = get_curr_dir();

        struct Command cmd;
        command_init(&cmd);

        // Prompt the user
        printf("%s@%s:%s%s ", user, host, cwd, delimiter);
        if (fgets(input, sizeof(input), stdin) == NULL) {
            // Handle ctrl-d by gracefully exiting
            printf("\n");
            break;
        }

        // Sanitize the input
        trim_newline(input);
        if (strlen(input) == 0) {
            check_bg_jobs(&bg_pids);
            continue;
        }

        // Parse the input
        read_command(input, &cmd);
        read_launch_ops(&cmd);

        // Handle buildins
        if (strcmp(cmd.sub_cmds[0].argv[0], "cd") == 0) {
            buildin_cd(cmd.sub_cmds[0].argv, home_dir);
            continue;
        } else if (strcmp(cmd.sub_cmds[0].argv[0], "exit") == 0) {
            buildin_exit();
        }

        // Start empty list of pipes
        bool success = true;
        struct List pipe_fds;
        list_init(&pipe_fds);

        // Construct the pipes
        int i;
        for (i = 0; i < cmd.num_sub_cmds - 1; i++) {
            int *fds = malloc(sizeof(int) * 2);
            if (pipe(fds) < 0) {
                perror("shell: pipe error");
                success = false;
                break;
            }
            list_push(&pipe_fds, fds);
        }

        // Clean up and move on if any pipe failed
        if (!success) {
            free_pipe_fds(&pipe_fds);
            command_free(&cmd);
            continue;
        }

        // Execute the commands
        for (i = 0; i < cmd.num_sub_cmds; i++) {
            pid_t child = fork();
            if (child < 0) {
                perror("shell: fork error");
                break;
            }

            if (child == 0) {
                execute_child(&cmd, &pipe_fds, i);
                fprintf(stderr, "%s: Command not found\n", cmd.sub_cmds[i].argv[0]);
            } else {
                if (cmd.num_sub_cmds == 1) {
                    if (cmd.bg) {
                        // Save pid to our background jobs list
                        int *id = malloc(sizeof(int));
                        *id = child;
                        list_push(&bg_pids, id);
                        printf("[%d]\n", (int) child);
                    } else {
                        waitpid(child, NULL, 0);
                    }
                } else if (i == 0) {
                    // Close the write pipe
                    int *fds = list_get(&pipe_fds, i)->data;
                    close(fds[1]);
                } else if (i + 1 == cmd.num_sub_cmds) {
                    // Close the read pipe
                    int *fds = list_get(&pipe_fds, i - 1)->data;
                    close(fds[0]);

                    if (cmd.bg) {
                        // Save pid to our background jobs list
                        int *id = malloc(sizeof(int));
                        *id = child;
                        list_push(&bg_pids, id);
                        printf("[%d]\n", (int) child);
                    } else {
                        waitpid(child, NULL, 0);
                    }
                } else {
                    // Close both read and write pipes
                    int *fds = list_get(&pipe_fds, i - 1)->data;
                    close(fds[0]);
                    fds = list_get(&pipe_fds, i)->data;
                    close(fds[1]);
                }
            }
        }

        // Clean up and move on to next input
        free_pipe_fds(&pipe_fds);
        command_free(&cmd);
    }

    free_bg_pids(&bg_pids);

    return 0;
}
