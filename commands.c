#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commands.h"

//===================================================================
// subcommand

void subcommand_init(struct SubCommand *sub) {
    sub->line = NULL;
}

void subcommand_free(struct SubCommand *sub) {
    if (sub->line != NULL) {
        free(sub->line);
    }

    char **argv = sub->argv;
    while (*argv != NULL) {
        free(*argv);
        argv++;
    }
}

void subcommand_print(struct SubCommand *sub) {
    int i = 0;
    char **argv = sub->argv;
    while (*argv != NULL) {
        printf("argv[%d] = '%s'\n", i, *argv);
        i++;
        argv++;
    }
}

//===================================================================
// command

void command_init(struct Command *cmd) {
    cmd->num_sub_cmds = 0;
    cmd->stdin_file = NULL;
    cmd->stdout_file = NULL;
    cmd->bg = false;
}

void command_free(struct Command *cmd) {
    int i;
    for (i = 0; i < cmd->num_sub_cmds; i++) {
        subcommand_free(&cmd->sub_cmds[i]);
    }

    if (cmd->stdin_file != NULL) {
        free(cmd->stdin_file);
    }

    if (cmd->stdout_file != NULL) {
        free(cmd->stdout_file);
    }
}

void command_print(struct Command *cmd) {
    int i;
    for (i = 0; i < cmd->num_sub_cmds; i++) {
        printf("Command %d:\n", i);
        subcommand_print(&cmd->sub_cmds[i]);
    }

    printf("\n");
    if (cmd->stdin_file != NULL) {
        printf("Redirect stdin: %s\n", cmd->stdin_file);
    } else {
        printf("Redirect stdin: NULL\n");
    }

    if (cmd->stdout_file != NULL) {
        printf("Redirect stdout: %s\n", cmd->stdout_file);
    } else {
        printf("Redirect stdout: NULL\n");
    }

    if (cmd->bg) {
        printf("Background: yes\n");
    } else {
        printf("Background: no\n");
    }
}

//===================================================================
// parsing

void read_argv(char *in, char **argv, int size) {
    // Create a copy of input since strtok mutates its argument
    char *buffer = strdup(in);
    if (buffer == NULL) {
        perror("strdup");
        return;
    }

    int current = 0;
    int max_index = size - 1;
    char *token = strtok(buffer, " ");

    while (token != NULL) {
        if (current >= max_index) {
            break;
        }

        char *arg = strdup(token);
        if (arg == NULL) {
            perror("strdup");
            return;
        }

        argv[current] = arg;
        current++;

        token = strtok(NULL, " ");
    }

    // Null-terminate the array
    argv[current] = NULL;

    free(buffer);
}

void read_command(char *line, struct Command *cmd) {
    // Create a copy of the line to manipulate.
    char *buffer = strdup(line);
    if (buffer == NULL) {
        perror("strdup");
        return;
    }

    int count = 0;
    char *token = strtok(buffer, "|");

    // Iterate through the tokens and save the sub-lines.
    while (token != NULL) {
        if (count >= MAX_SUB_COMMANDS) {
            break;
        }

        char *subline = strdup(token);
        if (subline == NULL) {
            perror("strdup");
            return;
        }

        struct SubCommand sub;
        subcommand_init(&sub);
        sub.line = subline;

        cmd->sub_cmds[count] = sub;
        count++;

        token = strtok(NULL, "|");
    }

    // Save number of subcommands and then parse args for each one.
    cmd->num_sub_cmds = count;

    int i;
    for (i = 0; i < cmd->num_sub_cmds; i++) {
        char *line = cmd->sub_cmds[i].line;
        char **argv = cmd->sub_cmds[i].argv;
        read_argv(line, argv, MAX_ARGS);
    }

    free(buffer);
}

void read_launch_ops(struct Command *cmd) {
    // Get the last subcommand
    int last = cmd->num_sub_cmds - 1;
    struct SubCommand *subcmd = &cmd->sub_cmds[last];

    // Find the last argument in sub-command
    int i = 0;
    while (subcmd->argv[i] != NULL) {
        i++;
    }
    i--;

    // Iterate through the arguments
    while (i >= 0) {
        if (strcmp(subcmd->argv[i], "&") == 0) {
            // Set background flag
            cmd->bg = true;

            free(subcmd->argv[i]);
            subcmd->argv[i] = NULL;
        } else if (strcmp(subcmd->argv[i], ">") == 0) {
            // Set redirect out file
            cmd->stdout_file = subcmd->argv[i+1];
            subcmd->argv[i+1] = NULL;

            free(subcmd->argv[i]);
            subcmd->argv[i] = NULL;
        } else if (strcmp(subcmd->argv[i], "<") == 0) {
            // Set redirect in file
            cmd->stdin_file = subcmd->argv[i+1];
            subcmd->argv[i+1] = NULL;

            free(subcmd->argv[i]);
            subcmd->argv[i] = NULL;
        }
        i--;
    }
}
