// Generic doubly linked list to store data for shell.
// Warning: data should be heap allocated or behavior will be undefined.

#pragma once

#include <stdbool.h>

struct Node {
    void *data;
    struct Node *next;
    struct Node *prev;
};

struct List {
    struct Node *head;
    struct Node *tail;
    int size;
};

// Initialize the list to defaults.
void list_init(struct List *ls);

// Free the nodes in the list.
// Warning: you must free the data yourself.
// Returns number of nodes freed.
int list_free(struct List *ls);

// Add data at specific location in list.
// Range: 0 <= index <= size
// When size == 0: insert as first item.
// When index == size: insert as last item.
// Returns the new node.
// Returns NULL if index out of range.
struct Node* list_add(struct List *ls, int index, void *data);

// Remove item at the specific location in list.
// Range: 0 <= index < size
// Returns the node.
// Returns NULL if index out of range.
struct Node* list_del(struct List *ls, int index);

// Get the item at specific location in list.
// Range: 0 <= index < size
// Returns the node.
// Returns NULL if index out of range.
struct Node* list_get(struct List *ls, int index);

// Add data to end of list.
// Returns node in list.
struct Node* list_push(struct List *ls, void *data);

// Remove last item in list.
// Returns the node.
// Returns NULL if empty.
struct Node* list_pop(struct List *ls);

// Add data to start of list.
// Returns node in list.
struct Node* list_enqueue(struct List *ls, void *data);

// Remove first item in list.
// Returns the node.
// Returns NULL if empty.
struct Node* list_dequeue(struct List *ls);

